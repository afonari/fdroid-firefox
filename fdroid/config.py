repo_url = "https://rfc2822.gitlab.io/fdroid-firefox/fdroid/"
repo_name = "Firefox F-Droid repo (unofficial)"
repo_icon = "fdroid-icon.png"
repo_description = """
Unofficial Firefox repo (.apk files from mozilla.org)
"""

archive_url = "https://does.not.exist"
archive_older = 1
archive_name = "Firefox archive repo (unofficial)"
archive_icon = "fdroid-icon.png"
archive_description = """
Unofficial Firefox archive repo (.apk files from mozilla.org)
"""

local_copy_dir = "/fdroid"

keystore = "../keystore.bks"
repo_keyalias = "rfc2822"
